<?php
  session_start();
  if($_SESSION['logged-in'] != true)
  { 
    header('Location: index.php');
    exit();
  }
  else if($_SESSION['logged-in'] == true)
  { 
    if($_SESSION['type'] == 0)
    {
      header('Location: user_proper.php');
      exit();
    } 
  }
?>

<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="css/style-main.css" rel="stylesheet" type="text/css"/>
  
  <meta keywords="szkoła, posiłek, posiłki, jedzenie, bułki" />
  <meta description="Strona w robocie!. Ambitny projekt, brak skilla" />

  <!--<link rel="icon" type="image/png" href="img/favico.png" />-->
  <link href="css/animation.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Courgette&amp;subset=latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Gafata&amp;subset=latin-ext" rel="stylesheet">

  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/main.js"></script>
        
</head>



<body>
  <div class="container-fluid wrapper">
    <header class="nav-top">
      <div class="logo">Stołówka</div>
    </header>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid nav-inside">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand white" href="#">Stołówka ZSK</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li><a href="#">Oferta</a></li>
            <li><a href="#">O nas</a></li>
            <li><a href="#">Kontakt</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="logout.php"><span class="glyphicon glyphicon-user"></span> Log out</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <script>
    $('#logbutton').click(function() {
          $('#logpanelid').toggle('slow', function() {
            // Animation complete.
          });
        });
    </script>
    <div class="logpanel" id="logpanelid">
      <form class="form-horizontal log" action="login.php" method="post">
        <div class="form-group row" >
          <label for="login">Login</label>
          <input type="text" name="login">
        </div>    
        <div class="form-group">
          <label for="pass">Hasło</label>
          <input type="password" name="pass">
        </div>
        <input class="login-button" type="submit" value="send">  
      </form>
    </div>
      <main class="row">
        <article class="main-column col-md-6 col-sm-12 col-xs-12">
          <div class="text-column">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue odio ipsum, id finibus mauris tincidunt vehicula. Ut vel massa eleifend, semper sapien eget, hendrerit nisl. Aliquam erat volutpat. Proin turpis velit, interdum in porttitor a, hendrerit sit amet risus. Sed mollis dictum mollis. Curabitur orci augue, sodales sit amet pellentesque vitae, porta non turpis. Nam eleifend luctus quam ac luctus. In sollicitudin odio velit, quis tempor massa pretium vitae. Suspendisse tincidunt erat eu cursus congue.
            </p>
            <p>
              Praesent finibus faucibus congue. Praesent eleifend rhoncus libero a suscipit. Aenean vestibulum sodales tempor. Aliquam sit amet imperdiet dui. Morbi posuere odio at libero egestas, vel tempus eros mattis. Etiam nulla ex, tempor eu maximus id, convallis sagittis nisl. In gravida a quam non malesuada. Morbi viverra dapibus elit quis malesuada. Praesent laoreet, tellus sit amet pharetra suscipit, massa sapien ultricies erat, non tincidunt elit arcu ac quam. Pellentesque sed nulla sollicitudin, egestas elit ut, mattis augue. Cras pulvinar, mauris in molestie gravida, odio arcu efficitur nunc, nec consectetur sem dolor vel tellus. Pellentesque cursus justo nibh, ac imperdiet massa congue congue. Aenean ultricies tellus id diam commodo pretium.
            </p>
          </div>
        </article>
         <article class="main-column col-md-6 col-sm-12 col-xs-12">
          <div class="form-feedback">
            <form class="form-horizontal" name="contactform" method="post" action="send_mail.php">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="first_name">Imię <span class="r">*</span></label>
                        <div class="col-md-6 col-sm-12">
                              <input class="form-control"  type="text" name="first_name"  maxlength="50">
                            </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="mail_from">E-mail <span class="r">*</span></label>
                        <div class="col-md-6 col-sm-12">
                                <input class="form-control"  type="text" name="mail_from"  maxlength="80">
                       </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="mail_sub">Tytuł<span class="r">*</span></label>
                      <div class="col-md-6 col-sm-12">
                        <input class="form-control"  type="text" name="mail_sub" maxlength="50">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="mail_msg">Uwagi<span class="r">*</span></label>
                      <div class="col-md-6 col-sm-12">
                        <textarea class="form-control"  name="mail_msg" maxlength="1000" cols="25" rows="8"></textarea>
                      </div>
                    </div>
                    <div class="col-md-offset-4 col-md-4">
                      <input class="form-control submit" type="submit" value="Wyślij">
                    </div>

              </form>

         </article>

      </main>

    <footer>
      <div class="row">
        <div class="col-md-offset-4 col-md-4 col-sm-12 infinity-name">Infinity Solutions</div>
        <div class="col-md-4">
          <img class="img-center" src="img/is-white-logo.png" alt="Infinity Solutions logo"/>
        </div>
      </div>
    </footer>
  </div>
  
</body>
</html>

