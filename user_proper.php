<?php
  session_start();
  if($_SESSION['logged-in'] != true)
  { 
    header('Location: index.php');
    exit();
  }
  else if($_SESSION['logged-in'] == true)
  { 
    if($_SESSION['type'] == 1)
    {
      header('Location: admin_proper.php');
      exit();
    } 
  }
?>

<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="css/style-main.css" rel="stylesheet" type="text/css"/>
  
  <meta keywords="szkoła, posiłek, posiłki, jedzenie, bułki" />
  <meta description="Strona w robocie!. Ambitny projekt, brak skilla" />

  <!--<link rel="icon" type="image/png" href="img/favico.png" />-->
  <link href="css/animation.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Courgette&amp;subset=latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Gafata&amp;subset=latin-ext" rel="stylesheet">

  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/main.js"></script>

  <style>
    .navbar-inverse
    {
      max-width: 90vw; 
      min-height: 60px
    }

    .navbar-brand
    {
      padding-left: 22px;
    }
    .nav-inside
    {
      padding: 12px;
    }
    .nav-top
    {
      height: 14vh;
    }
  </style>
        
</head>



<body>
  <div class="container-fluid wrapper">
    <header class="nav-top"></header>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid nav-inside">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand white" href="#">Stołówka ZSK</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

          <ul class="nav navbar-nav">
            <li><a href="#">Oferta</a></li>
            <li><a href="#">O nas</a></li>
            <li><a href="#">Kontakt</a></li>
          </ul>
          
          <ul class="nav navbar-nav navbar-right">
            <li><a href="logout.php"><span class="glyphicon glyphicon-user"></span> Log out</a></li>
          </ul>
        </div>
      </div>
    </nav>
    
      <main class="row">
        
        <article class="main-column col-md-6 col-sm-12 col-xs-12">
         
        </article>
        
        <article class="main-column col-md-6 col-sm-12 col-xs-12">
         

        </article>

      </main>

    <footer>
      <div class="row">
        
        <div class="col-md-offset-4 col-md-4 col-sm-12 infinity-name">Infinity Solutions</div>
        
        <div class="col-md-4">
          
          <img class="img-center" src="img/is-white-logo.png" alt="Infinity Solutions logo"/>

        </div>
      </div>
    </footer>
  </div>
  
</body>
</html>

