<?php
  session_start();
  if(isset($_SESSION['logged-in']))
  { 
    if($_SESSION['type'] == 1)
    {
      header('Location: admin_proper.php');
      exit();
    } 
    else if($_SESSION['type'] == 0)
    {
      header('Location: user_proper.php');
      exit();
    } 
  }
?>


<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Stołówka ZSK</title>
  <meta name="keywords" content="szkoła, posiłek, posiłki, jedzenie, bułki, zsk, zespół szkół, komunikacja, zespół szkół komunikacji">
  <meta name="description" content="Strona w robocie!. Ambitny projekt, brak skilla">
  <meta name="author" content="Mateusz Drewniak">

  <!--<link rel="icon" type="image/png" href="img/favico.png" />-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/animation.css" rel="stylesheet"/>
  <link href="css/fontello.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Courgette|Gafata|Lato|Varela" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script>
     $(document).ready(function() { 

      $('a[href^="#"]').on('click', function(event) {
  
       var target = $( $(this).attr('href') );
  
      if( target.length ) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
      }
     });

});
  </script>
        
</head>



<body>
  <div class="container-fluid wrapper">
    <header class="nav-top">
      <div id="logo">Stołówka</div>
    </header>
    <nav id="nav" class="navbar navbar-inverse">
      <div class="container-fluid nav-inside">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand white" href="#">Stołówka ZSK</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
          <!--onClick="window.scrollBy({ top: 2000, left: 0, behavior: 'smooth' });"-->
            <li><a href="#about">O nas</a></li>
            <li><a href="#email-form">Kontakt</a></li>
            <li><a href="#site-info">O stronie</a></li>
            <li><a href="#">Oferta</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="sign_up.php"><span class="glyphicon glyphicon-log-in"></span> Zarejestruj się</a></li>
            <li id="logbutton"><a><span class="glyphicon glyphicon-log-in"></span> Zaloguj się</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <script>
    $('#logbutton').click(function() {
          $('#logpanelid').toggle('fast', function() {
            // Animation complete.
          });
        });
   
    </script>
    <div class="logpanel" id="logpanelid" style="
    <?php
      if(isset($_SESSION['login-error']))
      {
        echo "display: block;";
      }
    ?>
    ">
      <form class="form-horizontal log" action="login.php" method="post">
        <div class="form-group row" >
          <label class="col-xs-3" for="login">Login</label>
          <input class="col-xs-7" type="text" name="login">
        </div>    
        <div class="form-group row">
          <label class="col-xs-3" for="pass">Hasło</label>
          <input class="col-xs-7" type="password" name="pass">
        </div>
        <input class="login-button" type="submit" value="Login">  
      </form>
      <?php
          if(isset($_SESSION['login-error']))
          {
              echo '<div style="margin-top:20px;" class="alert alert-danger alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$_SESSION['login-error'].'</div>';
              unset($_SESSION['login-error']);
          }

      ?>
    </div>
      <section class="row">
        <article class="main-column col-md-6 col-sm-12 col-xs-12">
          <div class="text-column" id="about">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue odio ipsum, id finibus mauris tincidunt vehicula. Ut vel massa eleifend, semper sapien eget, hendrerit nisl. Aliquam erat volutpat. Proin turpis velit, interdum in porttitor a, hendrerit sit amet risus. Sed mollis dictum mollis. Curabitur orci augue, sodales sit amet pellentesque vitae, porta non turpis. Nam eleifend luctus quam ac luctus. In sollicitudin odio velit, quis tempor massa pretium vitae. Suspendisse tincidunt erat eu cursus congue.
            </p>
            <p>
              Praesent finibus faucibus congue. Praesent eleifend rhoncus libero a suscipit. Aenean vestibulum sodales tempor. Aliquam sit amet imperdiet dui. Morbi posuere odio at libero egestas, vel tempus eros mattis. Etiam nulla ex, tempor eu maximus id, convallis sagittis nisl. In gravida a quam non malesuada. Morbi viverra dapibus elit quis malesuada. Praesent laoreet, tellus sit amet pharetra suscipit, massa sapien ultricies erat, non tincidunt elit arcu ac quam. Pellentesque sed nulla sollicitudin, egestas elit ut, mattis augue. Cras pulvinar, mauris in molestie gravida, odio arcu efficitur nunc, nec consectetur sem dolor vel tellus. Pellentesque cursus justo nibh, ac imperdiet massa congue congue. Aenean ultricies tellus id diam commodo pretium.
            </p>
          </div>
        </article>
         <article class="main-column col-md-6 col-sm-12 col-xs-12">
          <div class="form-feedback">
            <form id="email-form" class="form-horizontal" name="contactform" method="post" action="send_mail.php">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="first_name">Imię <span class="r">*</span></label>
                        <div class="col-md-6 col-sm-12">
                              <input class="form-control"  type="text" name="first_name"  maxlength="50">
                              <?php
                              if(isset($_SESSION['e_fname']))
                              {
                                  echo '<div class="alert alert-danger alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$_SESSION['e_fname'].'</div>';
                                  unset($_SESSION['e_fname']);
                              }
                              ?>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="mail_from">E-mail <span class="r">*</span></label>
                        <div class="col-md-6 col-sm-12">
                                <input class="form-control"  type="email" name="mail_from"  maxlength="80">
                                <?php
                              if(isset($_SESSION['e_mail']))
                              {
                                  echo '<div class="alert alert-danger alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$_SESSION['e_mail'].'</div>';
                                  unset($_SESSION['e_mail']);
                              }
                              ?>
                       </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="mail_sub">Tytuł<span class="r">*</span></label>
                      <div class="col-md-6 col-sm-12">
                        <input class="form-control"  type="text" name="mail_sub" maxlength="50">
                        <?php
                              if(isset($_SESSION['e_sub']))
                              {
                                  echo '<div class="alert alert-danger alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$_SESSION['e_sub'].'</div>';
                                  unset($_SESSION['e_sub']);
                              }
                              ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-12" for="mail_msg">Uwagi<span class="r">*</span></label>
                      <div class="col-md-6 col-sm-12">
                        <textarea class="form-control"  name="mail_msg" maxlength="1000" cols="10" rows="8"></textarea>
                        <?php
                              if(isset($_SESSION['e_msg']))
                              {
                                  echo '<div class="alert alert-danger alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$_SESSION['e_msg'].'</div>';
                                  unset($_SESSION['e_msg']);
                              }
                              ?>
                      </div>
                    </div>
                    <div class="col-md-offset-4 col-md-4">
                      <input class="form-control submit" type="submit" value="Wyślij">
                    </div>

              </form>
              <?php
                  if(isset($_SESSION['fb_message']))
                  {
                      echo '<div class="form-err-succ alert alert-danger alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$_SESSION['fb_message'].'</div>';
                      unset($_SESSION['fb_message']);
                  }
                  else if(isset($_SESSION['success_message']))
                  {
                      echo '<div class="form-err-succ alert alert-danger alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$_SESSION['success_message'].'</div>';
                      unset($_SESSION['success_message']);
                  }


              ?>

         </article>

      </section>
      <section class="food-img food1">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <i class="icon-food icon icon"></i>
            <p>Wysoka jakość</p>
            <p class="shift">Stołówka to miejsce z pysznym i zdrowym jedzeniem.</p>
          </div> 
          <div class="col-md-4 col-sm-12">
            <i class="icon-calendar icon"></i>
            <p>Zaplanuj posiłki</p>
            <p class="shift">Stwórz swój tygodniowy plan posiłków.</p>
          </div>
          <div class="col-md-4 col-sm-12"> 
            <i class="icon-bullhorn icon"></i>
            <p>Masz głos!</p>
            <p class="shift">Skorzystaj z formularza, aby skontaktować się z nami!</p>
          </div>
        </div>
      </section>

      <section id="site-info" class="row">
        <article class="main-column col-md-6 col-sm-12 col-xs-12">
          <div class="text-column">
            <p>Staramy się regularnie pracować nad ulepszaniem naszej witryny, tak aby korzystanie z niej było przyjemniejsze i zgodne z aktualnymi standardami. Jest to trudne zadanie, jeśli nie zna się potrzeb ludzi, do których kierowana jest strona. Jeśli chcesz wspomóc nas swoimi radami i pomóc nam lepiej dopasować możliwości aplikacji, możesz zrobić to za pomocą znajdującego sie obok formularza! Uzupełnij formularz swoim imieniem, mailem, dodaj tytuł oraz treść i możesz wysłać nam swoje uwagi! </p>
          </div>

          
        </article>
        <article class="main-column col-md-6 col-sm-12 col-xs-12">
          <div class="text-column">
            <p>Stołówka ZSK jest dostosowana do urządzeń wszelkiego rozmiaru. Chcesz zmienić plan posiłku, ale nie znajdujesz się w pobliżu swojego PC? Możesz to zrobic za pomocą smartfona, tableta, laptopa, bądź każdego innego urządzenia z dostępem do internetu i przeglądarką internetową. Komfort naszych uzytkowników stawiamy na pierwszym miejscu. </p>
          </div>

        </article>

      </section>

      <section class="food-img food2">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <i class="icon-laptop icon"></i>
            <p>Skorzystaj z laptopa</p>
            <p class="shift">Każde urządzenie mile widziane!</p>
          </div> 
          <div class="col-md-4 col-sm-12">
            <i class="icon-tablet icon"></i>
            <p>Tableta</p>
            <p class="shift">Może dotykowy ekran?</p>
          </div>
          <div class="col-md-4 col-sm-12"> 
            <i class="icon-mobile-1 icon"></i>
            <p>Bądź smartphona</p>
            <p class="shift">Jednak preferuję poręczność.</p>
          </div>
        </div>
      </section>

    <footer>
      <div class="row">
        <div class="col-md-offset-4 col-md-4 col-sm-12 infinity-name">Infinity Solutions</div>
        <div class="col-md-4">
          <img class="img-center" src="img/is-white-logo.png" alt="Infinity Solutions logo"/>
        </div>
      </div>
    </footer>
  </div>
  
</body>
</html>

