-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 21 Gru 2017, 08:49
-- Wersja serwera: 10.1.13-MariaDB
-- Wersja PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `stolowka`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `accounts`
--

CREATE TABLE `accounts` (
  `USER_ID` int(11) NOT NULL,
  `login` tinytext COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `email` tinytext COLLATE utf8_polish_ci NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `accounts`
--

INSERT INTO `accounts` (`USER_ID`, `login`, `password`, `email`, `type`) VALUES
(5, 'Mariusz', '$2y$10$EDUfzJEEw9BtJtU6JEYd5.6.iqumN/gYSyBn1V4iCPJ80IxaX0EZa', 'chujek@mgail.com', 1),
(6, 'Matiud', '$2y$10$gKeMG4ds8cYO8XF/2xxoaegK.oNCF0uHanEdyVhjeJBiMoOR2zK2S', 'jebaka@gmail.com', 0),
(7, 'Verseth', '$2y$10$GN9i8F1rwNZx06sototi8.T7aQjMFzLMOPf48TclwxEkea9fqg7rm', 'matidx2@poczta.onet.pl', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fri`
--

CREATE TABLE `fri` (
  `FRI_ID` int(11) NOT NULL,
  `FOOD_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `main_course` int(11) NOT NULL,
  `drink` int(11) NOT NULL,
  `dessert` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu`
--

CREATE TABLE `menu` (
  `FOOD_ID` int(11) NOT NULL,
  `f_name` tinytext NOT NULL,
  `f_description` text NOT NULL,
  `f_category` int(11) NOT NULL,
  `f_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mon`
--

CREATE TABLE `mon` (
  `MON_ID` int(11) NOT NULL,
  `FOOD_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `main_course` int(11) NOT NULL,
  `drink` int(11) NOT NULL,
  `dessert` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `thu`
--

CREATE TABLE `thu` (
  `THU_ID` int(11) NOT NULL,
  `FOOD_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `main_course` int(11) NOT NULL,
  `drink` int(11) NOT NULL,
  `dessert` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tue`
--

CREATE TABLE `tue` (
  `TUE_ID` int(11) NOT NULL,
  `FOOD_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `main_course` int(11) NOT NULL,
  `drink` int(11) NOT NULL,
  `dessert` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wed`
--

CREATE TABLE `wed` (
  `WED_ID` int(11) NOT NULL,
  `FOOD_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `main_course` int(11) NOT NULL,
  `drink` int(11) NOT NULL,
  `dessert` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`USER_ID`);

--
-- Indexes for table `fri`
--
ALTER TABLE `fri`
  ADD PRIMARY KEY (`FRI_ID`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`FOOD_ID`);

--
-- Indexes for table `mon`
--
ALTER TABLE `mon`
  ADD PRIMARY KEY (`MON_ID`);

--
-- Indexes for table `thu`
--
ALTER TABLE `thu`
  ADD PRIMARY KEY (`THU_ID`);

--
-- Indexes for table `tue`
--
ALTER TABLE `tue`
  ADD PRIMARY KEY (`TUE_ID`);

--
-- Indexes for table `wed`
--
ALTER TABLE `wed`
  ADD PRIMARY KEY (`WED_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `accounts`
--
ALTER TABLE `accounts`
  MODIFY `USER_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `fri`
--
ALTER TABLE `fri`
  MODIFY `FRI_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `menu`
--
ALTER TABLE `menu`
  MODIFY `FOOD_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `mon`
--
ALTER TABLE `mon`
  MODIFY `MON_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `thu`
--
ALTER TABLE `thu`
  MODIFY `THU_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `tue`
--
ALTER TABLE `tue`
  MODIFY `TUE_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `wed`
--
ALTER TABLE `wed`
  MODIFY `WED_ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
