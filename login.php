<?php
session_start();

if((!isset($_POST['login'])) || (!isset($_POST['pass'])))
{
	header('Location: index.php');
	exit();
}

require_once "connect.php";

$connection = @new mysqli($host, $db_user, $db_pass, $db_name);

if($connection->connect_errno!=0)
{
	echo "Error:".$connection->connect_errno;
}
else
{
	$login=$_POST['login'];
	$pass=$_POST['pass'];

	$login = htmlentities($login, ENT_QUOTES, "UTF-8");
	


	if($result = @$connection->query(sprintf("SELECT * FROM accounts WHERE login='%s'",
		mysqli_real_escape_string($connection,$login))))
	{
		$users = $result->num_rows;

		if($users==1)
		{
			$row = $result->fetch_assoc();
			
			if(password_verify($pass, $row['password']))
			{
				$_SESSION['logged-in'] = true;
				
				$_SESSION['user'] = $row['login'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['type'] = $row['type'];


				$result->close();
				
				if($_SESSION['type'] == 1)
				{
					header('Location: admin_proper.php');
				}
				header('Location: user_proper.php');
			}
			else
			{
				$connection->close();
				$_SESSION['login-error']='Nieprawidłowe dane!';
				header('Location: index.php#logo');
			}
			
		}
		else
		{
			$connection->close();
			$_SESSION['login-error']='Nieprawidłowe dane!';
			header('Location: index.php#logo');
		}
	}
	$connection->close();
}


?>


   

