<?php
  session_start();

  if(isset($_SESSION['logged-in']))
  { 
    if($_SESSION['type'] == 1)
    {
      header('Location: admin_proper.php');
      exit();
    } 
    else if($_SESSION['type'] == 0)
    {
      header('Location: user_proper.php');
      exit();
    } 
  }
  
  if(isset($_POST['email']))
  {
    $valid=true;

    $login=$_POST['login'];
    $email=$_POST['email'];
    $email_copy= filter_var($email, FILTER_SANITIZE_EMAIL);
    $pass1=$_POST['pass1'];
    $pass2=$_POST['pass2'];

    if((strlen($login)<5) || (strlen($login)>20))
    {
      $valid=false;
      $_SESSION['e_login']="Musisz podac login o prawidłowej długości";
      //Login is either too short or too long
    }

    elseif(ctype_alnum($login)==false)
    {
      $valid=false;
      $_SESSION['e_login']="Nieprawidłowa nazwa użytkownika";
      //Logins can only consist of alphanumerlas ;)
    }

    elseif((filter_var($email_copy, FILTER_VALIDATE_EMAIL)==false) || ($email_copy!=$email))
    {
      $valid=false;
      $_SESSION['e_email']="Nieprawidłowy email";
      //This email is impossible!
    }
    elseif((strlen($pass1)<8) || (strlen($pass1)>20))
    {
      $valid=false;
      $_SESSION['e_pass']="Nieprawidłowa długość hasła";
      //Improper length of the password
    }
    elseif($pass1!=$pass2)
    {
      $valid=false;
      $_SESSION['e_pass']="Hasła nie są identyczne";
      //Passwords are not identical
    }else{

    $pass_hash=password_hash($pass1, PASSWORD_DEFAULT);
    $secret = "6LdNdjoUAAAAAMGS4gXssEZShZcxZRs3S7ybgsrh";
    $check_captcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $response = json_decode($check_captcha);

    if(!isset($_POST['regulations']))
    {
      $valid=false;
      $_SESSION['e_reg']="Musisz zaakceptować nasz regulamin!";
      //One has to accept our policies
    }
    elseif($response->success==false)
    {
      $valid=false;
      $_SESSION['e_bot']="Udowodnij, że nie jesteś robotem";
      //Prove that you're not a robot!
    }

    require_once "connect.php";
    mysqli_report(MYSQLI_REPORT_STRICT);

    try
    {
      $connection = new mysqli($host, $db_user, $db_pass, $db_name);
      if($connection->connect_errno!=0)
      {
        throw new Exception(mysqli_conncect_errno());
      }
      else
      {
        //Is this email assigned to an existing account?
        $result = $connection->query("SELECT USER_ID FROM accounts WHERE email='$email'");
        if(!$result) throw new Exception($connection->error);

        $emails = $result->num_rows; //How many such emails exist in our database?
        if($emails>0)
        {
          $valid=false;
          $_SESSION['e_email']="Ten email jest już używany";
          //This email is already used by an account!
        }

        //Is this login assigned to an existing account?
        $result = $connection->query("SELECT USER_ID FROM accounts WHERE login='$login'");
        if(!$result) throw new Exception($conncection->error);

        $logins = $result->num_rows; //How many such logins exist in our database?
        if($logins>0)
        {
          $valid=false;
          $_SESSION['e_login']="Ten login jest juz zajęty";
          //This login is already used by an account!
        }

        if($valid==true)
        {
          if($connection->query("INSERT INTO accounts VALUES (NULL, '$login', '$pass_hash', '$email', 0)"))
          {
            $_SESSION['signed_up']=true;
            header('Location: greetings.php');
          }
          else
          {
            throw new Exception($connection->error);
          }
        }

        $connection->close();
      }
    }
    catch(Exception $error)
    {
      echo '<span style="color:red;">Błąd po stronie serwera! </span> <br> Further info for the pros: '.$error;
    }
  }
}

?>


<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Stołówka ZSK</title>
  <meta name="keywords" content="szkoła, posiłek, posiłki, jedzenie, bułki, zsk, zespół szkół, komunikacja, zespół szkół komunikacji">
  <meta name="description" content="Strona w robocie!. Ambitny projekt, brak skilla">
  <meta name="author" content="Mateusz Drewniak">

  <!--<link rel="icon" type="image/png" href="img/favico.png" />-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/animation.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
  <link href="css/fontello.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Courgette&amp;subset=latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Gafata&amp;subset=latin-ext" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/main.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
        
</head>

<body>
  <div class="container-fluid wrapper row">
    <div class="col-xs-12 col-sm-7 col-md-6 col-lg-5"> 
      <p class="signup-text">Dołącz do nas!</p>
      <form method="post" class="form-sign-up">
        <div class="form-group">
          <label for="login">Login</label> <br>
          <input class="form-control" type="text" id="login" name="login">
          <?php
            if(isset($_SESSION['e_login']))
            {
            echo "<div class='alert alert-danger alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>".$_SESSION['e_login']."</div>";

            unset($_SESSION['e_login']);
            }
          ?>
        </div>
        <div class="form-group">
          <label for="email">Adres Email</label> <br>
          <input class="form-control" type="email" id="email" name="email">
          <?php
            if(isset($_SESSION['e_email']))
            {
            echo "<div class='alert alert-danger alert-dismissible' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>".$_SESSION['e_email']."</div>";

            unset($_SESSION['e_email']);
            }
          ?>
        </div>
        <div class="form-group">
          <label for="pass1">Hasło</label> <br>
          <input class="form-control" type="password" id="pass1" name="pass1" aria-describedby="password-hint"> <br>
          <?php
            if(isset($_SESSION['e_pass']))
            {
            echo "<div class='alert alert-danger alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>".$_SESSION['e_pass']."</div>";
            unset($_SESSION['e_pass']);
            }
          ?>
          <small id="password-hint" class="form-text text-muted">Dłuższe niż 7 znaków. Musi zawierać małe i wielkie litery, cyfry oraz znaki specjalne.</small>
        </div>
        <div class="form-group">
          <label for="pass2">Powtórz Hasło</label> <br>
          <input class="form-control" type="password" name="pass2" id="pass2">
        </div>
        <div class="form-check">
          <div class="form-group">
            <label class="form-check-label">
              <input type="checkbox" class="form-check-input" name="regulations"> Akceptuję warunki regulaminu
            </label>
            <?php
            if(isset($_SESSION['e_reg']))
            {
            echo "<div class='alert alert-danger alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>".$_SESSION['e_reg']."</div>";
            unset($_SESSION['e_reg']);
            }
          ?>
          </div>
        </div>
        <div class="g-recaptcha" data-sitekey="6LdNdjoUAAAAAOp8cn-hM7TWbfX9lhKWOMdqslmF"></div>
        <button type="submit" class="btn btn-primary">Zarejestruj się</button>
      </form>
      <a class="btn btn-info" style="margin-top: 40px;" href="index.php" role="button">Powrót do strony głownej</a>
    </div>
    <div class="col-sm-5 col-md-6 col-lg-7 signup-img">
    </div>
  </div>
  
</body>
</html>

          